use std::fs::File;
use std::io::Read;
use std::env;
mod scanner;

fn main() {
    if env::args().count() == 1 {
        println!("REPL not yet supported");
        return;
    }
    let file_path = env::args().last();
    match file_path {
        None => panic!("REPL not supported yet"),
        Some(f) => {
            // For now the compiler will be hardcoded against the fibonacci example
            // TODO: Accept target file as an argument/drop into a REPL
            let mut f = File::open(f).expect("File not found");

            let mut contents = String::new();
            f.read_to_string(&mut contents).expect("File could not be read");

            let tokens = scanner::scan(&contents);
            println!("Tokens: {:#?}", tokens);
        }
    }
}
