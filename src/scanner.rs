struct Scanner {
    pub source: String,
    pub current: u16,
    pub line: u16,
    pub column: u16,
    pub source_len: u16,
}

#[derive(Debug)]
pub enum ReservedWord {
    Class,
    Else,
    If,
    Let,
    Loop,
    Mutable,
    Return,
    Until,
    While
}


#[derive(Debug)]
pub enum TokenType {
    Plus,
    Subtract,
    Slash,
    Asterisk,
    DoubleAsterisk,
    OpenBrace,
    CloseBrace,
    Pipe,
    OpenParenthesis,
    CloseParenthesis,
    Integer,
    Colon,
    Period,
    GreaterThan,
    GreaterThanEquals,
    LessThan,
    LessThanEquals,
    Bang,
    BangEquals,
    Question,
    String,
    Decimal,
    Identifier,
    Equals,
    FatArrow,
    ReservedWord(ReservedWord),
    EOF,
}

#[derive(Debug)]
pub struct Token {
    pub token_type: TokenType,
    pub line: u16,
    pub column: u16,
    pub lexeme: String,
}

struct ScanResult(String, TokenType);

// TODO: Tidy this up, review parsing strategy, ensure errors are thrown on syntax errors
impl Scanner {
    pub fn scan(&mut self) -> Vec<Token> {
        let mut vector = Vec::new();
        while self.current < self.source_len {
            let column = self.column + 1;
            let line = self.line;
            let c = self.advance();
            let result = match c {
                None => Some(ScanResult(String::from(""), TokenType::EOF)),
                Some(x) => {
                    match x {
                        '+' => Some(ScanResult(String::from("+"), TokenType::Plus)),
                        '-' => Some(ScanResult(String::from("-"), TokenType::Subtract)),
                        '/' => match self.peek() {
                            Some('/') => {
                                while self.peek() != Some('\n') {
                                    self.advance();
                                }
                                None
                            },
                           _ => Some(ScanResult(String::from("/"), TokenType::Slash)),
                        },
                        '{' => Some(ScanResult(String::from("{"), TokenType::OpenBrace)),
                        '}' => Some(ScanResult(String::from("}"), TokenType::CloseBrace)),
                        '|' => Some(ScanResult(String::from("|"), TokenType::Pipe)),
                        '(' => Some(ScanResult(String::from("("), TokenType::OpenParenthesis)),
                        ')' => Some(ScanResult(String::from(")"), TokenType::CloseParenthesis)),
                        '.' => Some(ScanResult(String::from("."), TokenType::Period)),
                        '?' => Some(ScanResult(String::from("?"), TokenType::Question)),
                        '*' => match self.peek() {
                            Some('*') => {
                                self.advance();
                                Some(ScanResult(String::from("**"), TokenType::DoubleAsterisk))
                            },
                            _ => Some(ScanResult(String::from("*"), TokenType::Asterisk))
                        },
                        ':' => Some(ScanResult(String::from(":"), TokenType::Colon)),
                        '>' => match self.peek() {
                            Some('=') => {
                                self.advance();
                                Some(ScanResult(String::from(">="), TokenType::GreaterThanEquals))
                            },
                            _ => Some(ScanResult(String::from(">"), TokenType::GreaterThan))
                        },
                        '<' => match self.peek() {
                            Some('=') => {
                                self.advance();
                                Some(ScanResult(String::from("<="), TokenType::LessThanEquals))
                            },
                            _ => Some(ScanResult(String::from("<"), TokenType::LessThan))
                        },
                        '!' => match self.peek() {
                            Some('=') => {
                                self.advance();
                                Some(ScanResult(String::from("!="), TokenType::BangEquals))
                            },
                            _ => Some(ScanResult(String::from("!"), TokenType::Bang))
                        },
                        '=' => match self.peek() {
                            Some('>') => {
                                self.advance();
                                Some(ScanResult(String::from("=>"), TokenType::FatArrow))
                            },
                            _ => Some(ScanResult(String::from("="), TokenType::Equals))
                        },
                        '"' => self.scan_string(x),
                        x if x >= '0' && x <= '9' => self.scan_number(x),
                        ' ' | '\t'=> None,
                        '\n' => {
                            self.line = self.line + 1;
                            self.column = 0;
                            None // Ignore whitespace
                        },
                        _ => Some(self.scan_identifier(x))
                    }
                }
            };
            match result {
                None => (),
                Some(x) => vector.push(
                    self.create_token(x, line, column)
                )
            }
        };
        vector.push(
            self.create_token(
                ScanResult(String::new(), TokenType::EOF),
                self.line,
                self.column
            )
        );
        vector
    }

    fn create_token(&self, x: ScanResult, line: u16, column: u16) -> Token {
        Token {
            token_type: x.1,
            line: line,
            column: column,
            lexeme: x.0
        }
    }

    fn scan_string(&mut self, lexeme: char) -> Option<ScanResult> {
        let mut string = String::new();
        string.push(lexeme);
        loop {
            match self.peek() {
                Some('"') => string.push(self.advance().unwrap()),
                _ => break
            }
        }
        Some(ScanResult(string, TokenType::String))
    }

    fn scan_number(&mut self, start: char) -> Option<ScanResult> {
        let mut string = String::new();
        string.push(start);
        loop {
            match self.peek() {
                Some('.') => {
                    string.push(self.advance().unwrap());
                    return self.scan_decimal(string)
                },
                Some(x) if x.is_numeric() || x == '_' => {
                    self.advance();
                    string.push(x)
                },
                None | Some(_) => break
            }
        }
        Some(ScanResult(string, TokenType::Integer))
    }

    fn scan_decimal(&mut self, mut decimal: String) -> Option<ScanResult> {
        loop {
            match self.peek() {
                Some(x) if x.is_numeric() || x == '_' => {
                    self.advance();
                    decimal.push(x)
                },
                None | Some(_) => break
            }
        }
        Some(ScanResult(decimal, TokenType::Decimal))
    }

    fn scan_identifier(&mut self, start: char) -> ScanResult {
        match self.scan_for_reserved_word(start) {
            Some(x) => x,
            None => {
                let mut identifier = String::new();
                identifier.push(start);
                loop {
                    match self.peek() {
                        Some(x) if self.is_valid_identifier_char(x) => {
                            identifier.push(self.advance().unwrap())
                        },
                        _ => break
                    }
                }
                ScanResult(identifier, TokenType::Identifier)
            }
        }
    }

    fn scan_for_reserved_word(&mut self, start: char) -> Option<ScanResult> {
        if start == 'c' && self.remaining_identifier_equals("lass") {
            for _ in 0..4 {
                self.advance();
            }
            Some(ScanResult(String::from("class"), TokenType::ReservedWord(ReservedWord::Class)))
        }  else if start == 'e' && self.remaining_identifier_equals("lse") {
            for _ in 0..4 {
                self.advance();
            }
            Some(ScanResult(String::from("else"), TokenType::ReservedWord(ReservedWord::Else)))
        } else if start == 'i' && self.remaining_identifier_equals("f") {
            for _ in 0..1 {
                self.advance();
            }
            Some(ScanResult(String::from("if"), TokenType::ReservedWord(ReservedWord::If)))
        } else if start == 'l' && self.remaining_identifier_equals("et") {
            for _ in 0..2 {
                self.advance();
            }
            Some(ScanResult(String::from("let"), TokenType::ReservedWord(ReservedWord::Let)))
        } else if start == 'l' && self.remaining_identifier_equals("oop") {
            for _ in 0..3 {
                self.advance();
            }
            Some(ScanResult(String::from("loop"), TokenType::ReservedWord(ReservedWord::Loop)))
        } else if start == 'm' && self.remaining_identifier_equals("ut") {
            for _ in 0..2 {
                self.advance();
            }
            Some(ScanResult(String::from("mut"), TokenType::ReservedWord(ReservedWord::Mutable)))
        } else if start == 'r' && self.remaining_identifier_equals("eturn") {
            for _ in 0..5 {
                self.advance();
            }
            Some(ScanResult(String::from("return"), TokenType::ReservedWord(ReservedWord::Return)))
        } else if start == 'u' && self.remaining_identifier_equals("ntil") {
            for _ in 0..4 {
                self.advance();
            }
            Some(ScanResult(String::from("until"), TokenType::ReservedWord(ReservedWord::Until)))
        } else if start == 'w' && self.remaining_identifier_equals("hile") {
            for _ in 0..4 {
                self.advance();
            }
            Some(ScanResult(String::from("while"), TokenType::ReservedWord(ReservedWord::While)))
        } else {
            None
        }
    }

    fn remaining_identifier_equals(&self, remainder: &str) -> bool {
        remainder.chars().enumerate().fold(true, {|acc, (i, c)|
            acc && self.lookahead(i as u16) == Some(c)
        })
    }

    fn is_valid_identifier_char(&self, x: char) -> bool {
        (x >= 'a' && x <= 'z') ||
            (x >= 'A' && x <= 'Z') ||
            (x >= '0' && x <= '9') ||
            x == '_'
    }

    fn lookahead(&self, i: u16) -> Option<char> {
        self.source.chars().nth(usize::from(self.current + i))
    }

    fn peek(&self) -> Option<char> {
        self.lookahead(0)
    }

    fn advance(&mut self) -> Option<char> {
        self.current = self.current + 1;
        self.column = self.column + 1;
        self.source.chars().nth(usize::from(self.current - 1))
    }
}

pub fn scan(source: &String) -> Vec<Token> {
    let mut scanner = Scanner {
        source: source.to_string(),
        current: 0,
        line: 1,
        column: 0,
        source_len: source.len() as u16
    };
    scanner.scan()
}
