# Lemon

Language name has not yet been committed to...

## Syntax

The language is aiming to have a similar syntax to something like JavaScript, however I think slightly more control should be given to the programmer, along with a type system and the ability to detect common errors _before_ runtime...

### Bindings

Bindings can be one of two states, mutable or immutable. By default all bindings are immutable and cannot be changed, the data inside a binding may be mutated but the binding will always point to the same object/value.

```
let name = "William"
let mut name = "William"
```

Bindings can also be shadowed so that although you do not mutate the binding, you can use the same identifier to access something else.

```
let x = 1
let x = 2
```

### Classes

Classes can be declared via the `class` keyword.

```
class Person {
}
```

Classes serve no use being empty though, they are capable of containing values within them. These are known as properties/fields and can be accessed via the `.` operator, they do not need the `.` in their names like methods do however because the language assumes that any time you `.` into an object but don't provide parantheses to invoke a method that you are trying to access a propery.

```
class Person {
  name: String;
}

let person = Person("William")
person.name // "William"
```

They are also capable of containing methods, this is done by prefixing an identifier with a `.` and then supplying arguments and a function body.

```
class Person {
  name: String;

  .equals(self, other: Person): Boolean {
    self.name.equals(other.name)
  }
}
```

As you can see, the method will receive the first argument as the object that `.equals` is being called on, and then the arguments passed in from the call site. The remaining arguments are type hinted as well as the method overall, so this method receives another instance of `Person` and returns a `Boolean`.

Why do we need to prefix functions with `.` though? Other languages allow `person.equals(other)` without having to include the `.` in the function signature right?

```
class PersonList {
  people: Array<People>;

  +(self, item: Person): PersonList {
    people + item
  }

  -(self, item: Person): PersonList {
    people - item
  }

  [i](self, index: Integer): Person {
    people[index]
  }
}

let mut list = PersonList()
let person = Person("William")

list = list + person // Add the person to the list
let person = list[0] // Get the first person in the list
list = list - person // Remove the person from the list
```

Not all methods you define on a class need to be made up of strings, numbers and underscores... Custom infix/prefix operators can be created, they do not need to be limited to the `+`/`-` shown above and you can also define custom logic for accessing indexes of a class if you want to.

It should be noted that the `.` character is not allowed to be used when defining an infix or prefix function, due to its use in resolving to normal methods or resolving fields.

```
class PersonList {
  people: Array<People>;

  +(self, item: Person): PersonList {
    people + item
  }

  -(self, item: Person): PersonList {
    people - item
  }

  contains(self, person: Person): Boolean {
    people.contains(person)
  }
}

let mut list = PersonList()
let person = Person()

list = list + person
list contains person // calls `contains` infix function, returns True
list = list - person
list contains person // now returns False
```

### Functions

Okay, so you can make classes and objects. Is this another Java/C# where everything has to be in a class?

Nope! Classes are not the only way to use the language, same as JavaScript/Ruby/PHP you are fully encouraged to decide how you want to write your program, with or without custom classes.

```
let fibonacci(i: Integer): Integer = {
  if i < 2 {
    i
  } else {
    fibonacci(i - 2) + fibonacci(i - 1)
  }
}

fibonaci(10)
```

Functions are just bindings that accept a value before they return anything, you can use the function syntax to create lazy evaluated values too.

```
let x(): Integer = 42
let x(): Integer = {
  42
}
```

In the above code, `x` will not have its value resolved until it's called the first time. After that _ideally_ it would return 42 without having to reevaluate the binding because it's completely pure and has already been done so once, but that may or may not get implemented due to the complexity.

### Anonymous functions

Functions can also be created without being manually bound to an identifier too, this might be useful for passing a single use function into a higher order function, such as when filtering or mapping a list

```
let person1 = Person("William")
let person2 = Person("Jim")
let people: Array<Person> = Array(person1, person2)
let names = people.map((person: Person) => person.name)
```

### Control Flow

The language provides a few constructs for control flow, already used within this document are the `if` and `else` keywords which work exactly as you would expect them to. There exists no `elseif` construct as the functionality is achieved by chaining `else` and `if` via `else if`. Unlike other C languages the need for curly braces is _enforced_ for `if` or `else`, so `else` can only be followed by `if` or `{`.


The language also intends to provide looping constructs to, while I prefer the usage of methods on arrays for looping that would not work for infinite looping until a given condition is met (traditionally a `while` loop).

The `while` keyword does not _quite_ work that way however, instead the `loop` keyword is used in one of 3 ways.

- `loop until i > 10 {}`
- `loop while i < 10 {}`
- `loop {}`

All of the above can be exited early either via a `return` or `break` expression, and they work as you would expect too. The first one will loop until the condition you specify is met, the second will loop as long as the condition is met and the last will loop indescriminately until `break` is used.


```
let value = loop {
  break 5
}
```

As the `loop` keyword is still an expression, it can return a value but in order to do this the value needs to be passed in when breaking the loop.

```
let mut i = 0;
let x = loop until i > 5 {
  i = i + 1
  i
}
```

For loops that can be exited naturally without a `break` you can also return values, the last expression in the last execution of the loop will be used as the value for whatever `let` binding is provided. Providing values via `break` is still allowed though.
